# Given a string in the format <n%>, return the decimal value of that percentage.
# Example: 37% is 0.37. 250% is 2.5.
def convert_percent_string(string):
    #string.strip('-')
    #string = string.strip('%')
    return abs(float(string.strip('%')))/100


def test_convert_percent_string():
    assert convert_percent_string("97%") == 0.97
    assert convert_percent_string("170%") == 1.7
    assert convert_percent_string("-33%") == 0.33
    assert convert_percent_string("0%") == 0


test_convert_percent_string()
