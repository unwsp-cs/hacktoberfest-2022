# Returns True if <value> is a valid smiley face.
# A smiley face is defined as a collection of characters consisting of "eyes", an optional "nose", and a "mouth."
# Eyes can be ":" or ";", nose can be "-" or "~", and smile can be "D" or ")" (feel free to tweak these rules)
# A smiley face must have eyes and a mouth, but a nose is not required
def is_valid_smiley(value):

    if len(value) > 1 and len(value) < 4:
        if value[-1] == ")" or value[-1] == "D":
            if value[0] == ":" or value[0] == ";":
                if len(value) == 3:
                    if value[1] == "-" or value[1] == "~":
                        return True
                else:
                    return True

    return False


def test_is_valid_smiley():
    assert not is_valid_smiley(":")
    assert not is_valid_smiley(")")
    assert is_valid_smiley(":D")
    assert is_valid_smiley(";-)")

def test_long_smiley():
    assert not is_valid_smiley(":      D")
    assert not is_valid_smiley(":--D")
    assert not is_valid_smiley(": D")

def test_noses():
    assert not is_valid_smiley(":o)")
    assert not is_valid_smiley(": )")

def test_extra_characters():
    assert not is_valid_smiley(":)q")
    assert not is_valid_smiley(":Dfldahfldhsaf")
    assert not is_valid_smiley("asdfs:D")
