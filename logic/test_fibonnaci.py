# Returns the Fibonacci number at the specified index.
# If you're unfamiliar with how the Fibonacci pattern works, search it up on your local search engine! :)
from _pytest.python_api import raises

def fibonacci(index):
    if index < 0 or not isinstance(index, int):
        raise ValueError
    elif index == 0:
        return 0
    elif index == 1 or index == 2:
        return 1
    return fibonacci(index-1) + fibonacci(index - 2)

def test_fibonacci():
    # original unit tests
    assert fibonacci(10) == 55
    assert fibonacci(25) == 75_025
    assert fibonacci(25) == 75_025

    # new unit tests
    assert fibonacci(0) == 0
    assert fibonacci(5) == 5
    assert fibonacci(29) == 514229
    with raises(ValueError):
        fibonacci(-1)
    with raises(ValueError):
        fibonacci(0.1)
