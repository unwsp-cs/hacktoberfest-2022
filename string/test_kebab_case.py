# Converts and returns the input to kebab-case
# kebab-case-looks-like-this!
# Add dashes in place of spaces and before capital letters.
def to_kebab_case(input):
    count = 0
    index = -1

    for i in input:
        count += 1
        index += 1

        if i.isupper() and index != 0:
            if " " in input[index - 1]:
                input = input[:index] + " " + input[index:]

            else:
                input = input[:count] + " " + input[count:]

    if "  " in input:
        input = input.replace("  ", " ")

    input = input.replace(" ", "-").lower()

    return input


def test_kebab_case():
    assert to_kebab_case("This is KebabCase") == "this-is-kebab-case"


def test_kebab_case2():
    assert to_kebab_case("This IsKebab case") == "this-is-kebab-case"


def test_kebab_case3():
    assert to_kebab_case("AlLpizza shouldHave Pineapple") == "all-pizza-should-have-pineapple"


def test_kebab_case4():
    assert to_kebab_case("I don't know What'sHere") == "i-don't-know-what's-here"
