# Given an input sentence, returns a modified sentence with all words reversed.
# "Hello world" -> "world Hello"
# "that car is red" -> "red is car that"
def reverse_sentence(sentence):
    words = sentence.split(' ')
    rev_sentence = ' '.join(reversed(words))

    return rev_sentence


def test_reverse_sentence():
    assert "the world spins in a circle" == "circle a in spins world the"
