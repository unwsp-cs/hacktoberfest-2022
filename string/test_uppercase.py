# Returns the input string in UPPERCASE.
def to_uppercase(string):
    return string.upper()

def test_to_uppercase():
    assert to_uppercase("turtles are my favorite animal") == "TURTLES ARE MY FAVORITE ANIMAL"

def test_to_uppercase2():
    assert to_uppercase("pizza is my favorite food") == "PIZZA IS MY FAVORITE FOOD"

def test_to_uppercase3():
    assert to_uppercase("basketball IS my favorite SPORT") == "BASKETBALL IS MY FAVORITE SPORT"

def test_to_uppercase4():
    assert to_uppercase("i love Computers") == "I LOVE COMPUTERS"

