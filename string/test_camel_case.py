# Converts and returns the input to camelCaseFormatting.
# In camelCase, the first character is underscored, and characters after spaces
# are uppercase.

from re import sub
def to_camel_case(input):
    input = sub(r"(_|-)", " ", input).title().replace(" ", "")
    return ''.join([input[0].lower(), input[1:]])


def test_snake_case():
    assert to_camel_case("Hello world how are you") == "helloWorldHowAreYou"
    assert to_camel_case("Hello world how are yo") == "helloWorldHowAreYo"
    assert to_camel_case("Hello world how are y") == "helloWorldHowAreY"
    assert to_camel_case("Hello world how are") == "helloWorldHowAre"
    assert to_camel_case("Hello world how ar") == "helloWorldHowAr"
    assert to_camel_case("Hello world how a") == "helloWorldHowA"
    assert to_camel_case("Hello world how") == "helloWorldHow"
    assert to_camel_case("Hello world ho") == "helloWorldHo"
