# Converts and returns the input to snake_case_formatting.
# In snake case, all spaces are replaced with underscores,
# and all text is lowercase.
# For bonus points, add underscores before any capital letters!

from re import sub
def to_snake_case(input):

    return '_'.join(
        sub('([A-Z][a-z]+)', r' \1',
            sub('([A-Z]+)', r' \1',
                input.replace('-', ' '))).split()).lower()
    return input


def test_snake_case():
    assert to_snake_case("Pizza Is AWESOME") == "pizza_is_awesome"
