from builtins import type, str


# Returns true if all elements in the collection are of 'str' type.
# Hint: check the imports in this file to figure out how to do this!
def check_all_string(collection):
    #print("The data types are: ", type(str(collection)))
    return all(isinstance(i, str) for i in collection)



def test_check_all_string():
    assert not check_all_string([1, 2, "foo"])
    assert check_all_string(["a", "b"])
    assert not check_all_string([3.33, 6.67, 1024])
    assert check_all_string(["-99.99", "2048", "int"])