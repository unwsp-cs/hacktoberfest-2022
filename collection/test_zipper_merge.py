# Zipper merge collections <a> and <b>, and assume they are the same length / non-empty. Start with <a>.
def zipper_merge(a, b):

    loops = 0
    c = []  #[item for sublist in zip(a, b) for item in sublist]
    while len(c) < 2*len(a):

        c.insert(len(c),a[loops])
        c.insert(len(c),b[loops])
        print(c)

        loops +=1
    return c



def test_zipper_merge():
    assert zipper_merge(["a", "b", "c"], [1, 2, 3]) == ["a", 1, "b", 2, "c", 3]

def test_zipper_merge2():
    assert zipper_merge(["c", "b", "a"], [3, 2, 1]) == ["c", 3, "b", 2, "a", 1]