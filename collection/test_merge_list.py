# Returns an array/list containing all elements from a and b.
# merge_list(['a', 'b', 'c'], ['d', 'e', 'f']) == ['a', 'b', 'c', 'd', 'e', 'f']
def merge_list(a, b):
    list = a + b
    return list


def test_merge_lists():
    assert merge_list(['a', 'b', 'c'], ['d', 'e', 'f']) == ['a', 'b', 'c', 'd', 'e', 'f']
    assert merge_list(['d', 'e', 'f'], ['g', 'h', 'i']) == ['d', 'e', 'f', 'g', 'h', 'i']
    assert merge_list(['g', 'h', 'i'], ['j', 'k', 'l']) == ['g', 'h', 'i', 'j', 'k', 'l']
    assert merge_list(['j', 'k', 'l'], ['m', 'n', 'o']) == ['j', 'k', 'l', 'm', 'n', 'o']
    assert merge_list(['m', 'n', 'o'], ['p', 'q', 'r']) == ['m', 'n', 'o', 'p', 'q', 'r']
    assert merge_list(['p', 'q', 'r'], ['s', 't', 'u']) == ['p', 'q', 'r', 's', 't', 'u']
    assert merge_list(['s', 't', 'u'], ['v', 'w', 'x']) == ['s', 't', 'u', 'v', 'w', 'x']
