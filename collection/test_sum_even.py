# Returns the sum of all even numbers within the provided collection.

def sum_all_even(collection):

    total = 0
    # check if number in list is even. if so, add it to total
    for number in collection:
        if number%2 == 0:
            total += number

    return total

def test_sum_all_even():
    assert sum_all_even([1, 4, 4, 6, 7, 9, 2, 0]) == 16

def test_sum_even_negative():
    assert sum_all_even([1, 3, -4, 8, -9, 1, 3, 1]) == 4

def test_not_sum_all_even():
    assert sum_all_even([1, 4, 4, 6, 7, 9, 2, 0]) != 15

def test_sum_single():
    assert sum_all_even([4]) == 4

def test_sum_all_odd():
    assert sum_all_even([1, 3, 77, 9, 5]) == 0