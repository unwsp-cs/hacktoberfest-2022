# Given a <collection> of elements, return a new collection with the elements reversed in order.
# Note: you're prohibited from using the .reverse() function! :D
def flip_flop(collection):
    collection_L=len(collection)
    new_collection = []
    for n in range(collection_L):
        new_collection.insert(0,collection[n])
    collection = new_collection
    return collection


def test_flip_flop():
    assert flip_flop([1, 2, 3, 4, 5]) == [5, 4, 3, 2, 1]
    assert flip_flop(["a", "b", 5]) == [5, "b", "a"]

    assert flip_flop(["this","is","backwards"]) == ["backwards","is","this"]
    assert flip_flop([2,"z",13]) == [13,"z",2]
    assert flip_flop(["a","b","c","d"]) == ["d","c","b","a"]
