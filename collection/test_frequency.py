# Returns the most frequently occurring element in this collection.
# If the input collection is empty, returns None.
# If any elements appear the same number of times (and are the most frequent), return None.
import statistics
import pytest


def get_most_frequent(collection):
    if not collection:
        return None
    mode = statistics.multimode(collection)
    if mode.__len__() > 1:
        return None
    return mode.__getitem__(0)


def test_get_most_frequent():
    assert get_most_frequent([4, 4, 4, 2, 3]) == 4
    assert get_most_frequent([4, 4]) == 4
    assert get_most_frequent([4]) == 4
    assert get_most_frequent([1, 1, 2, 2, 3]) is None
    assert get_most_frequent([]) is None


def test_get_most_frequent_for_exceptions():
    with pytest.raises(TypeError):
        get_most_frequent("fr")
    with pytest.raises(TypeError):
        get_most_frequent(123)

