# Returns the value rounded to the nearest whole integer.

def round1(value):
    value = round(value)
    return value

def test_round():
    assert round1(5.4) == 5
    assert round1(5.6) == 6
    assert round1(0.00001) == 0
    assert round1(808080.8080) == 808081
    assert round1(0.49999999) == 0