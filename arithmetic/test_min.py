def minimum(x, y):
    if x <= y:
        return x
    else:
        return y


def test_min():
    assert minimum(5, 10) == 5


def test_min1():
    assert minimum(10, 5) == 5


def test_min2():
    assert minimum(5, 5) == 5

