# Returns the result of subtracting x from y.
def subtract_numbers(x, y):
    return x - y


def test_subtract_numbers():
    assert subtract_numbers(32, 31) == 2


def test_subtract_numbers1():
    assert subtract_numbers(1000000, 999998) == 2
