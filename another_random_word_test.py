# Given string <sentence>, return a random word from that sentence.
# You can remove punctuation and capitalization when processing the sentence.
def get_random_word(string):

    import random
    words = (string.split())
    rand=(random.choice(words).split())
    print('Your random word is: ', rand)
    return rand[0]


def test_random_word():
    assert get_random_word("hello octoberfest we are almost done") in ["hello", "octoberfest", "we", "are", "almost", "done"]

